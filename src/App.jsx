import React from 'react'
import { useState, useEffect } from 'react'
import PropTypes from 'prop-types';
import './App.scss';
import ModalImage from './components/ModalImage/ModalImage'
import ModalText from './components/ModalText/ModalText'
import ShopPage from './components/ShopPage/ShopPage';
import Header from './components/Header/Header';
import MainPage from './components/MainPage/MainPage';


const App = () => {
  
  const [modalToShow, setModalToShow] = useState({
    active: false,
    type: null
  });
  const [productName, setProductName] = useState('');
  const [productDescription, setProductDescription] = useState('');
  const [cart, setCart] = useState([]);
  const [star, setStar] = useState([]);
  const [items, setItems] = useState([]);



  useEffect(() => {
      fetch('products.json')
          .then(response => response.json())
          .then(data => setItems(data));
  }, [])



  useEffect(() => {
    const storedCart = JSON.parse(localStorage.getItem('cart')) || [];
    const storedStar = JSON.parse(localStorage.getItem('star')) || [];
    const storedItems = JSON.parse(localStorage.getItem('items')) || [];

    setCart(storedCart);
    setStar(storedStar);
    setItems(storedItems);
  }, []);

  useEffect(() => {
      localStorage.setItem('cart', JSON.stringify(cart));
      localStorage.setItem('star', JSON.stringify(star));
      localStorage.setItem('items', JSON.stringify(items));
  }, [cart, star, items]);
  
  useEffect(() => {
    star.map(id => {
        localStorage.setItem(`starClick-${id}`, 'true');
        return null;
    });
}, [star]);
  
  function addToCart(id) {
    if (!cart.includes(id)) {
      const newCart = [...cart, id]
      setCart(newCart);
      showModal('text', productName, productDescription);
    }
  }

  function addToStar(id) {
    if (!star.includes(id)) {
      const newStar = [...star, id]
      setStar(newStar);
    }
    else {
        const newStar = star.filter(itemId => itemId !== id);
        setStar(newStar);
    }
  }



  function showModal(type, productName, productDescription) {
    setModalToShow({
      active: true,
      type: type
    });
    setProductName(productName);
    setProductDescription(productDescription);
  }


  function closeModal() {
    setModalToShow({
      active: false,
      type: null
    })
  }

  
  return (

    <>
      <Header cart={cart} star={star} />
      <MainPage />
      <ShopPage items={items} showModal={showModal} addToCart={addToCart} addToStar={addToStar} />
      {modalToShow.type === 'image' && (
        <ModalImage
          active={modalToShow.active}
          setActive={closeModal}
          firstText={"NO, CANCEL"}
          secondaryText={'YES, DELETE'}
          firstClick={closeModal}
          secondaryClick={closeModal}
        />
      )}
      {modalToShow.type === 'text' && (
        <ModalText
          active={modalToShow.active}
          setActive={closeModal}
          firstText={'ADD TO CART'}
          isSecondModal={true}
          firstClick={() => addToCart(modalToShow, productName, productDescription)}
          productName={productName}
          productDescription={productDescription}
          showModal={showModal}
        />
      )}

    </>
  );
  
}

export default App;
