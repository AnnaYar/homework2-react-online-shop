import Button from '../Button/Button';
import './ProductCard.scss'
import { useState, useEffect } from 'react';
import StarIcon from '../StarIcon/StarIcon';
import PropTypes from 'prop-types';



const ProductCard = (props)=> {

    const [starClick, setStarClick] = useState(false);

     useEffect(() => {
        const storedStarClick = localStorage.getItem(`starClick-${props.id}`);
        setStarClick(storedStarClick === 'true');
    }, [props.id]);

    function handleStarClick(event) {
        event.preventDefault();
        const newStarClick = !starClick;
    setStarClick(newStarClick);
        localStorage.setItem(`starClick-${props.id}`, newStarClick.toString());
        props.addToStar(props.id);
    }
        return (
            <div className="product">
                <a className='product__link' href="#">
                    <img className="product__image" src={props.imageUrl} alt=""/>
                    <div className="product__icon">
                        <StarIcon starClick={starClick} handleStarClick={handleStarClick} /> 
                    </div>
                    <div className='product__main-info'>
                        <h3 className="product__name">{props.name}</h3>
                    </div>
                    <div className='product__description'>
                        <div>
                            <p className="product__vendor-code">{props.sku}</p>
                            <p className="color">{props.color}</p>
                            <p className="product__price">{props.price} UAH</p>
                        </div>
                        <Button onClick={(event) => {
                            event.preventDefault();
                            props.showModal('text', props.name, props.description);
                        }} className='btn btn-card'>Add to cart</Button>
                    </div>
                </a>
            </div>
        )
}
    
ProductCard.propTypes = {
    id: PropTypes.string,               
    imageUrl: PropTypes.string,        
    name: PropTypes.string,            
    sku: PropTypes.toString,             
    color: PropTypes.string,           
    price: PropTypes.number,           
    description: PropTypes.string,                
    addToStar: PropTypes.func,         
    showModal: PropTypes.func          
};

ProductCard.defaultProps = {
    description: '',  
};

export default ProductCard;
