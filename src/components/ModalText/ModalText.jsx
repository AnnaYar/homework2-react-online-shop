import Modal from '../Modal/Modal'
import ModalBody from '../ModalBody/ModalBody'
import PropTypes from 'prop-types'; 



const ModalText = ({active, setActive, firstText, firstClick, isSecondModal, productName, productDescription})=> {

  return (
    <>
      <Modal
        active={active}
        setActive={setActive}
        firstText={firstText}
        firstClick={firstClick}
        isSecondModal={isSecondModal}
        productName={productName}>
        <ModalBody>
          <h2 className="modal-body__title modal-body__title_magin-second-modal">Add {productName}</h2>
          <p className='modal-body__text modal-body__text_margin-second-modal'>{productDescription}</p>
        </ModalBody>
      </Modal>
    </>
    )
}

ModalText.propTypes = {
    active: PropTypes.bool.isRequired,              
    setActive: PropTypes.func.isRequired,           
    firstText: PropTypes.string,                    
    firstClick: PropTypes.func,                     
    isSecondModal: PropTypes.bool,                  
    productName: PropTypes.string.isRequired,       
    productDescription: PropTypes.string.isRequired 
};

ModalText.defaultProps = {
    firstText: '',              
    firstClick: () => {},       
    isSecondModal: false,       
};

export default ModalText;
