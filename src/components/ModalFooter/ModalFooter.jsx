import Button from '../Button/Button'
import PropTypes from 'prop-types';
import './ModalFooter.scss'
import '../Button/Button.scss'

const ModalFooter = ({ firstText, secondaryText, firstClick, secondaryClick, isSecondModal }) => {
    
    return (
        <>
            <div className={`modal-footer ${isSecondModal ? 'second-modal-footer' : ''}`}>
                {firstText && <Button className='btn-violet' onClick={firstClick}>{firstText}</Button>}
                {secondaryText && <Button onClick={secondaryClick}>{secondaryText}</Button>}
            </div>
        </>
    )
}

ModalFooter.propTypes = {
    firstText: PropTypes.string,        
    secondaryText: PropTypes.string,    
    firstClick: PropTypes.func,         
    secondaryClick: PropTypes.func,     
    isSecondModal: PropTypes.bool,      
};

ModalFooter.defaultProps = {
    firstText: '',                      
    secondaryText: '',                  
    firstClick: () => {},               
    secondaryClick: () => {},           
    isSecondModal: false,               
};

export default ModalFooter;
