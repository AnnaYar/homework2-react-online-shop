import ProductCard from "../ProductCard/ProductCard";
import PropTypes from 'prop-types';
import './ShopPage.scss';


const ShopPage = ({items, showModal, addToCart, addToStar}) => {

    return (
        <>
            <div className="shop">
                {items.map((product) =>
                    <ProductCard
                        imageUrl={product.imageUrl}
                        name={product.name}
                        sku={product.sku}
                        color={product.color}
                        price={product.price}
                        key={product.id}
                        id={product.id}
                        description={product.description}
                        showModal={showModal}
                        addToCart={addToCart}
                        addToStar={addToStar}
                    />
                )
                }
        </div>
        </>
    )  
}

ShopPage.propTypes = {
    items: PropTypes.arrayOf(
        PropTypes.shape({
            id: PropTypes.string,       
            imageUrl: PropTypes.string, 
            name: PropTypes.string,     
            sku: PropTypes.string,      
            color: PropTypes.string,    
            price: PropTypes.number,   
            description: PropTypes.string,        
        })
    ), 
    showModal: PropTypes.func,  
    addToCart: PropTypes.func,  
    addToStar: PropTypes.func,  
};

export default ShopPage;
