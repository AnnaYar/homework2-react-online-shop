import React from "react";
import PropTypes from 'prop-types';
import './Button.scss'


const Button = ({type="button", className, onClick, children}) => {

    return (
        <button type={type} className={`btn ${className}`} onClick={onClick} >
            {children}
        </button>
    );
};

Button.propTypes = {
    type: PropTypes.string,
    className: PropTypes.string,
    onClick: PropTypes.func,
    children: PropTypes.node.isRequired,
}

Button.defaultProps = {
    type: "button",                 
    className: "",                  
    onClick: () => {},              
};

export default Button;