import React from "react";
import ModalWrapper from "../ModalWrapper/ModalWrapper";
import ModalHeader from "../ModalHeader/ModalHeader";
import ModalClose from '../ModalClose/ModalClose'
import ModalBody from '../ModalBody/ModalBody'
import ModalFooter from '../ModalFooter/ModalFooter'
import PropTypes from 'prop-types';


const Modal = ({active, setActive, children, firstText, secondaryText, firstClick, secondaryClick, isSecondModal}) => {
    return (
        <ModalWrapper active={active} setActive={setActive}>
            <ModalHeader >
                <ModalClose onClick={() => setActive(false)}/>
            </ModalHeader>
            <ModalBody>
                {children}
            </ModalBody>
            <ModalFooter
                firstText={firstText}
                secondaryText={secondaryText}
                firstClick={() => { firstClick(); setActive(false); }}
                secondaryClick={secondaryClick}
                isSecondModal={isSecondModal} />
        </ModalWrapper>
                
    );
};

Modal.propTypes = {
    active: PropTypes.bool.isRequired,           
    setActive: PropTypes.func.isRequired,        
    children: PropTypes.node,                    
    firstText: PropTypes.string,                 
    secondaryText: PropTypes.string,             
    firstClick: PropTypes.func,                  
    secondaryClick: PropTypes.func,              
    isSecondModal: PropTypes.bool,               
};

Modal.defaultProps = {
    children: null,                              
    firstText: '',                               
    secondaryText: '',                           
    firstClick: () => {},                        
    secondaryClick: () => {},                    
    isSecondModal: false,                        
};


export default Modal;