import './ModalBody.scss'
import PropTypes from 'prop-types';

const ModalBody = ({children }) => {
    return (
        <>
            <div className='modal-body'>
                {children}
            </div>
        
        </>
    )
}

ModalBody.propTypes = {
    children: PropTypes.node,  
};

ModalBody.defaultProps = {
    children: null,  
};

export default ModalBody;