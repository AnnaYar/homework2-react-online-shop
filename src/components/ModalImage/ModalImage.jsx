import ModalImg from '../../assets/macbook.jpeg'
import Modal from '../Modal/Modal'
import ModalBody from '../ModalBody/ModalBody'
import PropTypes from 'prop-types';


const ModalImage = ({active, setActive, firstText, secondaryText, firstClick, secondaryClick}) => {
    
    return (
        <>
            <Modal active={active} setActive={setActive} firstText={firstText} secondaryText={secondaryText} firstClick={firstClick} secondaryClick={secondaryClick}>
                <ModalBody>
                    <img className='modal-body__img' src={ModalImg} width = '276' height='140' alt="" />
                    <h2 className="modal-body__title modal-body__title_magin-first-modal">Product Delete!</h2>
                    <p className='modal-body__text modal-body__text_margin-first-modal'>By clicking the “Yes, Delete” button, PRODUCT NAME will be deleted</p>
                </ModalBody>
            </Modal>
            
        </>
    )
}

ModalImage.propTypes = {
    active: PropTypes.bool.isRequired,            
    setActive: PropTypes.func.isRequired,         
    firstText: PropTypes.string,                 
    secondaryText: PropTypes.string,              
    firstClick: PropTypes.func,                   
    secondaryClick: PropTypes.func,               
};

ModalImage.defaultProps = {
    firstText: '',                               
    secondaryText: '',                           
    firstClick: () => {},                        
    secondaryClick: () => {},                   
};

export default ModalImage;